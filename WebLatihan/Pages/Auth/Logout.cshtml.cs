using System;
using System.Linq;
using WebLatihan.Constans;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authentication;
using WebLatihan.Enums;

namespace WebLatihan.Pages.Auth
{
    [Authorize(Roles = UserRoles.Admin + "," + UserRoles.User)]
    public class LogoutModel : PageModel
    {
        public async Task<IActionResult> OnGet()
        {
            if(User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync(LatihanAuthenticationSchemes.Cookie);
            }
            return Redirect("~/Auth/Login");
        }
    }
}