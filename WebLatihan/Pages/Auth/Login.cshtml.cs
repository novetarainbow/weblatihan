using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebLatihan.Enums;
using WebLatihan.Models;
using WebLatihan.Services;

namespace WebLatihan.Pages.Auth
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly LatihanService _LatihanMan;
        public LoginModel(LatihanService latihanService)
        {
            _LatihanMan = latihanService;
        }

        [BindProperty]
        public LoginFormModel Form { get; set; }

        public string RoleDb { get; set; }
        public string Email { get; set; }

        [TempData]
        public string SuccessMessage { get; set; }
        
        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync(String returnUrl)
        {
            if(User.Identity.IsAuthenticated)
            {
                return Redirect("~/");
            }
            if(ModelState.IsValid == false)
            {
                return Page();
            }

            var loginDb = await _LatihanMan.GetLogin(Form.Username);
            if(loginDb == null)
            {
                SuccessMessage = "Username/Password salah";
                return Page();
            }

            var usernameDb = loginDb.Username;
            var passwordDb = loginDb.PasswordUser;
            RoleDb = loginDb.RoleUser;
            Email = loginDb.Email;
            if(loginDb.IsActive==false)
            {
                SuccessMessage = "Account belum di aktivasi, silahkan cek email";
                return Page();
            }

            var isUseMatched = Form.Username == usernameDb;
            var isPassMatched = _LatihanMan.Verify(Form.Password, passwordDb);

            var isCredentialsOk = isUseMatched && isPassMatched;
            if(isCredentialsOk==false)
            {
                SuccessMessage = "invalid username or password";
                return Page();
            }

            var claims = GenerateClaims();
            var persistence = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(5),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(LatihanAuthenticationSchemes.Cookie, claims, persistence);
            if(string.IsNullOrEmpty(returnUrl) == false)
            {
                return LocalRedirect(returnUrl);
            }
            return Redirect("~/");
        }

        private ClaimsPrincipal GenerateClaims()
        {
            var claims = new ClaimsIdentity(LatihanAuthenticationSchemes.Cookie);
            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.Username));
            claims.AddClaim(new Claim(ClaimTypes.Email, Email));
            claims.AddClaim(new Claim(ClaimTypes.Role, RoleDb));

            return new ClaimsPrincipal(claims);
        }
    }
}