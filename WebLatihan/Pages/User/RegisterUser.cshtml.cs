using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebLatihan.Services;
using WebLatihan.Models;

namespace WebLatihan.Pages.User
{
    [AllowAnonymous]
    public class RegisterUserModel : PageModel
    {
        private readonly LatihanService _LatihanService;
        //dependency injection buat manggil kelas lain
        public RegisterUserModel(LatihanService latihanService)
        {
            _LatihanService = latihanService;
        }
        [TempData]
        public string SuccessMessage { get; set; }

        [BindProperty(SupportsGet = true)]//mendapatkan data yg user inputkan
        public UserViewModel Form { get; set; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid == false)
            {
                return Page();

            }
            var checkExistUsername = await _LatihanService.CheckExistUsername(Form.Username);
            if(checkExistUsername==false)
            {
                ModelState.AddModelError("Form.Username", "Username Already Exist");
                return Page();
            }
            var checkExistEmail = await _LatihanService.CheckExistEmail(Form.Email);
            if (checkExistEmail == false)
            {
                ModelState.AddModelError("Form.Email", "Email Already Exist");
                return Page();
            }
            var regUser = await _LatihanService.InsertUser(Form);
            if(regUser == false)
            {
                SuccessMessage = "Register User Failed";
                return Page();

            }
            SuccessMessage = "Registration Successfull";
            return Page();
        }
    }
}