﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebLatihan.Constans;
using WebLatihan.Entities;
using WebLatihan.Models;

namespace WebLatihan.Services
{
    public class LatihanService
    {
        private readonly PostGreDbContext _PostGreDbContext;
        private readonly AppSettings _AppSetings;

        //dependency injection agar bisa manggil kelas lain
        public LatihanService(PostGreDbContext postGreDbContext, AppSettings appSettings)
        {
            _PostGreDbContext = postGreDbContext;
            _AppSetings = appSettings;
        }

        /// <summary>
        /// Hash Password using bcrypt
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public string Hash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, 12); //12 kekuatan enkripsi password saat ini yg tertinggi 12
        }

        /// <summary>
        /// buat verify
        /// </summary>
        /// <param name="notHash"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        public bool Verify(string notHash, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(notHash, hash);
        }

        /// <summary>
        /// cek username ada atau blm
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistUsername(string username)
        {
            var checkExist = await _PostGreDbContext.TbUser
                                    .Where(Q => Q.UserName == username)
                                    .CountAsync();
            if(checkExist>0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// cek email ada atau blm
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<bool> CheckExistEmail(string email)
        {
            var checkExist = await _PostGreDbContext.TbUser
                                    .Where(Q => Q.Email == email)
                                    .CountAsync();
            if (checkExist > 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> InsertUser(UserViewModel model)
        {
            var activationLink = Hash(model.Username).Replace("/", "");
            activationLink = activationLink.Replace(".", "");
            var addData = new WebLatihan.Entities.TbUser()
            {
                UserName = model.Username,
                PasswordUser = Hash(model.PasswordUser),
                RoleUser = UserRoles.User,
                Email = model.Email,
                IsActive = true,//false,
                ActivationLink = activationLink
            };
            _PostGreDbContext.TbUser.Add(addData);
            await _PostGreDbContext.SaveChangesAsync();
            //await SendMail(PurposeEnum.ActivateUser, model.Email, activationLink);
            return true;
        }

        public async Task<LoginDbModel> GetLogin(string username)
        {
            var getData = await this._PostGreDbContext.TbUser
                .Where(Q => Q.UserName == username)
                .Select(Q => new LoginDbModel
                {
                    UserId = Q.UserId,
                    Username = Q.UserName,
                    PasswordUser = Q.PasswordUser,
                    RoleUser = Q.RoleUser,
                    Email = Q.Email,
                    IsActive = Q.IsActive
                }).FirstOrDefaultAsync();
            return getData;
        }
    }
}
