﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebLatihan.Services
{
    public class AppSettings
    {
        public CredentialsSetting Credentials { get; set; }
    }

    public class CredentialsSetting
    {
        public string SmtpEmail { get; set; }
        public string PasswordSmtpEmail { get; set; }
    }
}
