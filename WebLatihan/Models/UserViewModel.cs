﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLatihan.Models
{
    public class UserViewModel
    {
        [Required(ErrorMessage = "Harus diisi!")]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Harus diisi!")]
        [StringLength(255, MinimumLength = 8)]
        [Display(Name = "Password")]
        public string PasswordUser { get; set; }

        [Compare("PasswordUser", ErrorMessage = "Passwprd tidak sama!")]
        [Required(ErrorMessage = "Harus diisi!")]
        [StringLength(255, MinimumLength = 8)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPasswordUser { get; set; }

        [Required(ErrorMessage = "Harus diisi!")]
        [StringLength(255, MinimumLength = 5)]
        [Display(Name = "e-Mail")]
        public string Email { get; set; }
    }
}
