﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebLatihan.Enums
{
    public class PurposeEnum
    {
        public const string ActivateUser = "ActivateUser";
        public const string ForgotPassword = "ForgotPassword";
    }
}
