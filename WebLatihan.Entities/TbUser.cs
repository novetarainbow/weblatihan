﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebLatihan.Entities
{
    public partial class TbUser
    {
        public int UserId { get; set; }
        [Required]
        [StringLength(20)]
        public string UserName { get; set; }
        [Required]
        [StringLength(255)]
        public string PasswordUser { get; set; }
        [Required]
        [StringLength(20)]
        public string RoleUser { get; set; }
        [Required]
        [StringLength(255)]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string ActivationLink { get; set; }
        [StringLength(255)]
        public string ForgotPasswordLink { get; set; }
    }
}
