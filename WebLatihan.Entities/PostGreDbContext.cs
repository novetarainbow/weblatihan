﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebLatihan.Entities
{
    public partial class PostGreDbContext : DbContext
    {
        //hapust public postgredbcontext()
        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbUser> TbUser { get; set; }

        //hapus on cofiguring()

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_User");

                entity.HasIndex(e => e.ActivationLink)
                    .HasName("TbUser_ActivationLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Email)
                    .HasName("TbUser_Email_key")
                    .IsUnique();

                entity.HasIndex(e => e.ForgotPasswordLink)
                    .HasName("TbUser_ForgotPasswordLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.UserName)
                    .HasName("TbUser_UserName_key")
                    .IsUnique();

                entity.Property(e => e.UserId).UseNpgsqlIdentityAlwaysColumn();
            });
        }
    }
}
